Application Name: Landmarks

This is a very simply iOS application. Upon opening, a list of landmarks around the world is displayed in a UITableView. This list is generated using a plist dictionary. When the TableViewCell is tapped, it loads a UIWebView of the Wiki page for the landmark.