//
//  LandmarkWebViewController.h
//  Landmarks
//
//  Created by Barbara Brown on 10/10/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LandmarkWebViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *webView;

@property (strong, nonatomic) NSString *websiteURL;
@property (nonatomic, strong) NSString *landmarkName;

@end
