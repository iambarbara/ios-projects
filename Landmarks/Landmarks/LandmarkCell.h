//
//  LandmarkCell.h
//  Landmarks
//
//  Created by Barbara Brown on 10/10/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LandmarkCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *landmarkImageView;
@property (nonatomic, strong) IBOutlet UILabel *landmarkName;
@property (nonatomic, strong) IBOutlet UILabel *countryLabel;
@property (nonatomic, strong) IBOutlet UILabel *cityLabel;

@end
