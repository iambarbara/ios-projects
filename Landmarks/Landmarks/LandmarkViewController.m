//
//  ViewController.m
//  Landmarks
//
//  Created by Barbara Brown on 10/10/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "LandmarkViewController.h"
#import "LandmarkCell.h"
#import "LandmarkWebViewController.h"

@interface LandmarkViewController ()

@property (nonatomic, strong) NSString *websiteURL;
@property (nonatomic, strong) NSString *landmarkTitle;

@end

@implementation LandmarkViewController

- (void)viewDidLoad
{
    //---------------------------------------------------------------------------------------------------
    
    // filePath is declared as a local variable of character string type
    NSString *filePath;   // File path to the plist file in the application's main bundle (project folder)
    
    //---------------------------------------------------------------------------------------------------
    // Obtain the file path to the Landmarks.plist file
    filePath = [[NSBundle mainBundle] pathForResource:@"Landmarks" ofType:@"plist"];
    self.landmarksDict = [[NSDictionary alloc] initWithContentsOfFile:filePath];
    
    /*
     Send allKeys message to the dictionary object and obtain all of the keys (landmark names) in an array.
     Send the sortedArrayUsingSelector: message to that array. It returns an array that lists the receiver’s
     elements in ascending order, as determined by the comparison method specified by a given selector.
     */
    self.landmarkNames = [[self.landmarksDict allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
    [super viewDidLoad];   // Inform super
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Data Source Methods

// Although the default is 1; it is still good to include this method for clarity
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Asks the data source to return the number of rows in a given section.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.landmarkNames count];
}

// Asks the data source to return a cell to insert in a particular table view location
- (LandmarkCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger rowNumber = [indexPath row];
    
    NSString *landmarkName = [self.landmarkNames objectAtIndex:rowNumber];
    NSArray *landmarkData = [self.landmarksDict objectForKey:landmarkName];
    
    LandmarkCell *cell = (LandmarkCell *)[tableView dequeueReusableCellWithIdentifier:@"LandmarkCellType"];
    
    
    cell.landmarkName.text = landmarkName;
    cell.landmarkImageView.image = [UIImage imageNamed:[landmarkData objectAtIndex:0]];
    cell.countryLabel.text = [landmarkData objectAtIndex:2];
    cell.cityLabel.text = [landmarkData objectAtIndex:1];
    
    return cell;
}


#pragma mark - Table View Delegate Methods

//       Asks the table view delegate to return the height of a given row.
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kTableViewRowHeight;
}


//       Informs the table view delegate that the table view is about to display a cell for a particular row.
//       Just before the cell is displayed, we change the color of the cell's background.

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //   We use the modulus operator % to find the remainder of the division of row number by 2.
    //   If the remainder is 0, it is an even number (use MintCream); otherwise, it is an odd number (use OldLace).
    
    if (indexPath.row % 2 == 0) {
        // Set even numbered row background color to MintCream, #F5FFFA  245,255,250
        cell.backgroundColor = MINT_CREAM;
    } else {
        // Set odd numbered row background color to OldLace, #FDF5E6   253,245,230
        cell.backgroundColor = OLD_LACE;
    }
}


// Informs the table view delegate that the user tapped the detail accessory button
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger rowNumber = [indexPath row];
    NSString *landmarkName = [self.landmarkNames objectAtIndex:rowNumber];
    self.landmarkTitle = landmarkName;
    self.landmarkData = [self.landmarksDict objectForKey:landmarkName];
    self.websiteURL = [self.landmarkData objectAtIndex:3];
    
    // Perform the segue named LandmarkWebView
    [self performSegueWithIdentifier:@"LandmarkWebView" sender:self];
}


#pragma mark - Preparing for Segue

// This method is called by the system whenever you invoke the method performSegueWithIdentifier:sender:
// You never call this method. It is invoked by the system.
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"LandmarkWebView"]) {
        
        // Obtain the object reference of the destination view controller
        LandmarkWebViewController *landmarkWebViewController = [segue destinationViewController];
        
        // Pass the data objects landmarkName and websiteURL to the downstream view controller LandmarkWebViewController
        landmarkWebViewController.landmarkName = self.landmarkTitle;
        landmarkWebViewController.websiteURL = self.websiteURL;
    }
}


@end
