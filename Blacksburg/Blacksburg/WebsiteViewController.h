//
//  BTWebsiteViewController.h
//  Blacksburg
//
//  Created by Barbara Brown on 10/2/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebsiteViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSString *URL;
@property (strong, nonatomic) IBOutlet UINavigationItem *navLabel;

@end
