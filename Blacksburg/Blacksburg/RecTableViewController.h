//
//  RecTableViewController.h
//  Blacksburg
//
//  Created by Barbara Brown on 10/2/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecTableViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UITableView *recTableView;
@property (strong, nonatomic) NSArray *navMenu;
@end
