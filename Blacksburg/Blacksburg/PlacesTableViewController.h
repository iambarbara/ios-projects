//
//  PlacesTableViewController.h
//  Blacksburg
//
//  Created by Barbara Brown on 10/2/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlacesTableViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UITableView *placesTableView;
@property (strong, nonatomic) NSArray *navMenu;
@end
