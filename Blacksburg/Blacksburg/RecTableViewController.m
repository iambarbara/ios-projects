//
//  RecTableViewController.m
//  Blacksburg
//
//  Created by Barbara Brown on 10/2/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "RecTableViewController.h"
#import "WebsiteViewController.h"

@interface RecTableViewController ()
@property (strong, nonatomic) NSString *urlOfWebsite;
@property (strong, nonatomic) NSString *navLabel;
@end

@implementation RecTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.recTableView.dataSource = self;
    self.recTableView.delegate = self;
    self.navMenu = [[NSArray alloc] initWithObjects:@"Aquatic Center Hours", @"Athletic Fields", @"Community Center Hours", @"Ellett Valley Recreational Hours", @"Heritage Community Park and Natural Area", @"Huckleberry Trail", @"Municipal Golf Course Programs", @"Municipal Park", nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.navMenu count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"RecReuseID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSString *menu = [self.navMenu objectAtIndex:indexPath.row];
    [cell.textLabel setText:menu];
    cell.imageView.image = [UIImage imageNamed:@"bInCircle30"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[self.navMenu objectAtIndex:indexPath.row] isEqualToString:@"Aquatic Center Hours"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=1518";
        self.navLabel = @"Aquatic Center Hours";
        [self performSegueWithIdentifier:@"RecWebsite" sender:self];
    }
    else if ([[self.navMenu objectAtIndex:indexPath.row] isEqualToString:@"Athletic Fields"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=767";
        self.navLabel = @"Athletic Fields";
        [self performSegueWithIdentifier:@"RecWebsite" sender:self];
    }
    else if ([[self.navMenu objectAtIndex:indexPath.row] isEqualToString:@"Community Center Hours"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=586";
        self.navLabel = @"Community Center Hours";
        [self performSegueWithIdentifier:@"RecWebsite" sender:self];
    }
    else if ([[self.navMenu objectAtIndex:indexPath.row] isEqualToString:@"Ellett Valley Recreational Hours"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=1437";
        self.navLabel = @"Ellett Valley Recreational Hours";
        [self performSegueWithIdentifier:@"RecWebsite" sender:self];
    }
    else if ([[self.navMenu objectAtIndex:indexPath.row] isEqualToString:@"Heritage Community Park and Natural Area"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=1164";
        self.navLabel = @"Heritage Community Park and Natural Area";
        [self performSegueWithIdentifier:@"RecWebsite" sender:self];
    }
    else if ([[self.navMenu objectAtIndex:indexPath.row] isEqualToString:@"Huckleberry Trail"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=808";
        self.navLabel = @"Huckleberry Trail";
        [self performSegueWithIdentifier:@"RecWebsite" sender:self];
    }
    else if ([[self.navMenu objectAtIndex:indexPath.row] isEqualToString:@"Municipal Golf Course Programs"]){
        self.urlOfWebsite = @"http://www.golfthehill.com/golf-programs";
        self.navLabel = @"Municipal Golf Course Programs";
        [self performSegueWithIdentifier:@"RecWebsite" sender:self];
    }
    else if ([[self.navMenu objectAtIndex:indexPath.row] isEqualToString:@"Municipal Park"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=1860";
        self.navLabel = @"Municipal Park";
        [self performSegueWithIdentifier:@"RecWebsite" sender:self];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"RecWebsite"]) {
        WebsiteViewController *websiteViewController = [segue destinationViewController];
        websiteViewController.URL = self.urlOfWebsite;
        [websiteViewController.navLabel setTitle:self.navLabel];
    }
}

@end
