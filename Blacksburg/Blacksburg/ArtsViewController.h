//
//  ArtsViewController.h
//  Blacksburg
//
//  Created by Barbara Brown on 10/3/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@end
