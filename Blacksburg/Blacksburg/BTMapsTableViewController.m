//
//  BTGenInfoTableViewController.m
//  Blacksburg
//
//  Created by Barbara Brown on 10/2/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "BTMapsTableViewController.h"
#import "WebsiteViewController.h"

@interface BTMapsTableViewController ()

@property (strong, nonatomic) NSString *urlOfWebsite;
@property (strong, nonatomic) NSString *mapsInfoNavLabel;

@end

@implementation BTMapsTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.btMapsTableView.dataSource = self;
    self.btMapsTableView.delegate = self;
    self.mapsMenu = [[NSArray alloc] initWithObjects:@"CRC/Hospital", @"Harding Avenue", @"Hethwood", @"Hokie Express", @"Main Street", @"Patrick Henry", @"Toms Creek", @"Progress Street", @"Two Town Trolley", @"University City Boulevard", @"University Mall", nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.mapsMenu count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"MapsInfoReuseID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSString *menu = [self.mapsMenu objectAtIndex:indexPath.row];
    [cell.textLabel setText:menu];
    cell.imageView.image = [UIImage imageNamed:@"bInCircle30"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[self.mapsMenu objectAtIndex:indexPath.row] isEqualToString:@"CRC/Hospital"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=1034";
        self.mapsInfoNavLabel = @"CRC/Hospital";
        [self performSegueWithIdentifier:@"MapsWebsite" sender:self];
    }
    else if ([[self.mapsMenu objectAtIndex:indexPath.row] isEqualToString:@"Harding Avenue"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=1024";
        self.mapsInfoNavLabel = @"Harding Avenue";
        [self performSegueWithIdentifier:@"MapsWebsite" sender:self];
    }
    else if ([[self.mapsMenu objectAtIndex:indexPath.row] isEqualToString:@"Hethwood"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=1024";
        self.mapsInfoNavLabel = @"Hethwood";
        [self performSegueWithIdentifier:@"MapsWebsite" sender:self];
    }
    else if ([[self.mapsMenu objectAtIndex:indexPath.row] isEqualToString:@"Hokie Express"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=993";
        self.mapsInfoNavLabel = @"Hokie Express";
        [self performSegueWithIdentifier:@"MapsWebsite" sender:self];
    }
    else if ([[self.mapsMenu objectAtIndex:indexPath.row] isEqualToString:@"Main Street"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=1010";
        self.mapsInfoNavLabel = @"Main Street";
        [self performSegueWithIdentifier:@"MapsWebsite" sender:self];
    }
    else if ([[self.mapsMenu objectAtIndex:indexPath.row] isEqualToString:@"Patrick Henry"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=1005";
        self.mapsInfoNavLabel = @"Patrick Henry";
        [self performSegueWithIdentifier:@"MapsWebsite" sender:self];
    }
    else if ([[self.mapsMenu objectAtIndex:indexPath.row] isEqualToString:@"Toms Creek"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=1000";
        self.mapsInfoNavLabel = @"Toms Creek";
        [self performSegueWithIdentifier:@"MapsWebsite" sender:self];
    }
    else if ([[self.mapsMenu objectAtIndex:indexPath.row] isEqualToString:@"Progress Street"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=1353";
        self.mapsInfoNavLabel = @"Progress Street";
        [self performSegueWithIdentifier:@"MapsWebsite" sender:self];
    }
    else if ([[self.mapsMenu objectAtIndex:indexPath.row] isEqualToString:@"Two Town Trolley"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=1743";
        self.mapsInfoNavLabel = @"Two Town Trolley";
        [self performSegueWithIdentifier:@"MapsWebsite" sender:self];
    }
    else if ([[self.mapsMenu objectAtIndex:indexPath.row] isEqualToString:@"University City Boulevard"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=1004";
        self.mapsInfoNavLabel = @"University City Boulevard";
        [self performSegueWithIdentifier:@"MapsWebsite" sender:self];
    }
    else if ([[self.mapsMenu objectAtIndex:indexPath.row] isEqualToString:@"University Mall"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=1001";
        self.mapsInfoNavLabel = @"University Mall";
        [self performSegueWithIdentifier:@"MapsWebsite" sender:self];
    }
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"MapsWebsite"]) {
        WebsiteViewController *btWebsiteViewController = [segue destinationViewController];
        btWebsiteViewController.URL = self.urlOfWebsite;
        [btWebsiteViewController.navLabel setTitle:self.mapsInfoNavLabel];
        
    }
}

@end
