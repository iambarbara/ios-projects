//
//  BTViewController.h
//  Blacksburg
//
//  Created by Barbara Brown on 10/2/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTTableViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UITableView *btTableView;
@property (strong, nonatomic) NSArray *btMenu;
@end
