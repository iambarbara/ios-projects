//
//  BTViewController.m
//  Blacksburg
//
//  Created by Barbara Brown on 10/2/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "BTTableViewController.h"
#import "WebsiteViewController.h"
#import "BTMapsTableViewController.h"

@interface BTTableViewController ()

@property (strong, nonatomic) NSString *urlOfWebsite;
@property (strong, nonatomic) NSString *btNavLabel;

@end

@implementation BTTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.btTableView.dataSource = self;
    self.tableView.delegate = self;
    self.btMenu = [[NSArray alloc] initWithObjects:@"Home", @"General Info", @"Schedules and Maps", @"BT Access", @"Fares and Passes", @"Kid's Stop", @"Want To", nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.btMenu count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"BTReuseID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSString *menu = [self.btMenu objectAtIndex:indexPath.row];
    [cell.textLabel setText:menu];
    cell.imageView.image = [UIImage imageNamed:@"bInCircle30"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[self.btMenu objectAtIndex:indexPath.row] isEqualToString:@"Home"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=791";
        self.btNavLabel = @"Home";
        [self performSegueWithIdentifier:@"BTWebsite" sender:self];
    }
    else if ([[self.btMenu objectAtIndex:indexPath.row] isEqualToString:@"General Info"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=884";
        self.btNavLabel = @"General Info";
        [self performSegueWithIdentifier:@"BTWebsite" sender:self];
    }
    else if ([[self.btMenu objectAtIndex:indexPath.row] isEqualToString:@"Schedules and Maps"]){
        [self performSegueWithIdentifier:@"MapsInfoNav" sender:self];
    }
    else if ([[self.btMenu objectAtIndex:indexPath.row] isEqualToString:@"BT Access"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=886";
        self.btNavLabel = @"BT Access";
        [self performSegueWithIdentifier:@"BTWebsite" sender:self];
    }
    else if ([[self.btMenu objectAtIndex:indexPath.row] isEqualToString:@"Fares and Passes"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=1068";
        self.btNavLabel = @"Fares and Passes";
        [self performSegueWithIdentifier:@"BTWebsite" sender:self];
    }
    else if ([[self.btMenu objectAtIndex:indexPath.row] isEqualToString:@"Kid's Stop"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=888";
        self.btNavLabel = @"Kid's Stop";
        [self performSegueWithIdentifier:@"BTWebsite" sender:self];
    }
    else if ([[self.btMenu objectAtIndex:indexPath.row] isEqualToString:@"Want To"]){
        self.urlOfWebsite = @"http://www.blacksburg.gov/Index.aspx?page=889";
        self.btNavLabel = @"Want To";
        [self performSegueWithIdentifier:@"BTWebsite" sender:self];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"BTWebsite"]) {
        WebsiteViewController *websiteViewController = [segue destinationViewController];
        websiteViewController.URL = self.urlOfWebsite;
        [websiteViewController.navLabel setTitle:self.btNavLabel];
    }
//    else if ([[segue identifier] isEqualToString:@"MapsInfoNav"]) {
//        BTMapsTableViewController *btMapsViewController = [segue destinationViewController];
//  }
    
}

@end
