//
//  PlacesTableViewController.m
//  Blacksburg
//
//  Created by Barbara Brown on 10/2/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "PlacesTableViewController.h"
#import "WebsiteViewController.h"

@interface PlacesTableViewController ()

@property (strong, nonatomic) NSString *urlOfWebsite;
@property (strong, nonatomic) NSString *navLabel;

@end

@implementation PlacesTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.placesTableView.dataSource = self;
    self.placesTableView.delegate = self;
    self.navMenu = [[NSArray alloc] initWithObjects:@"Restaurants", @"Hotels", @"Parking", @"Bars", nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.navMenu count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"PlacesReuseID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSString *menu = [self.navMenu objectAtIndex:indexPath.row];
    [cell.textLabel setText:menu];
    cell.imageView.image = [UIImage imageNamed:@"bInCircle30"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[self.navMenu objectAtIndex:indexPath.row] isEqualToString:@"Restaurants"]){
        self.urlOfWebsite = @"http://maps.google.com/?q=24060+Restaurants";
        self.navLabel = @"Restaurants";
        [self performSegueWithIdentifier:@"PlacesWebsite" sender:self];
    }
    else if ([[self.navMenu objectAtIndex:indexPath.row] isEqualToString:@"Hotels"]){
        self.urlOfWebsite = @"http://maps.google.com/?q=24060+Hotels";
        self.navLabel = @"Hotels";
        [self performSegueWithIdentifier:@"PlacesWebsite" sender:self];
    }
    else if ([[self.navMenu objectAtIndex:indexPath.row] isEqualToString:@"Parking"]){
        self.urlOfWebsite = @"http://maps.google.com/?q=24060+Parking";
        self.navLabel = @"Parking";
        [self performSegueWithIdentifier:@"PlacesWebsite" sender:self];
    }
    else if ([[self.navMenu objectAtIndex:indexPath.row] isEqualToString:@"Bars"]){
        self.urlOfWebsite = @"http://maps.google.com/?q=24060+Bars";
        self.navLabel = @"Bars";
        [self performSegueWithIdentifier:@"PlacesWebsite" sender:self];
    }
    }

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"PlacesWebsite"]) {
        WebsiteViewController *websiteViewController = [segue destinationViewController];
        websiteViewController.URL = self.urlOfWebsite;
        [websiteViewController.navLabel setTitle:self.navLabel];
    }
}

@end
