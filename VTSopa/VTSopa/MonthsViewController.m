//
//  EventsViewController.m
//  VTSopa
//
//  Created by Barbara Brown on 10/30/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "MonthsViewController.h"
#import "MonthCell.h"
#import "EventsViewController.h"

@interface MonthsViewController ()

@property (strong, nonatomic) NSString *eventNavLabel;

@end

@implementation MonthsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];   // Inform super
    
    self.navigationItem.title = @"Season Events";
    CGRect frame = CGRectMake(0, 0, 75, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:20]];
    label.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView = label;
    label.text = self.navigationItem.title;
    
    self.monthsMenu = [[NSArray alloc] initWithObjects:@"September", @"October", @"November", @"December", @"January", @"February", @"March", @"April", @"May", nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.monthsMenu count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"MonthCellType";
    MonthCell *cell = (MonthCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSString *menu = [self.monthsMenu objectAtIndex:indexPath.row];
    cell.monthLabel.text = menu;
    cell.monthImage.image = [UIImage imageNamed:@"calendar"];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[self.monthsMenu objectAtIndex:indexPath.row] isEqualToString:@"September"])
        self.eventNavLabel = @"September";
    else if ([[self.monthsMenu objectAtIndex:indexPath.row] isEqualToString:@"October"])
        self.eventNavLabel = @"October";
    else if ([[self.monthsMenu objectAtIndex:indexPath.row] isEqualToString:@"November"])
        self.eventNavLabel = @"November";
    else if ([[self.monthsMenu objectAtIndex:indexPath.row] isEqualToString:@"December"])
        self.eventNavLabel = @"December";
    else if ([[self.monthsMenu objectAtIndex:indexPath.row] isEqualToString:@"January"])
        self.eventNavLabel = @"January";
    else if ([[self.monthsMenu objectAtIndex:indexPath.row] isEqualToString:@"February"])
        self.eventNavLabel = @"February";
    else if ([[self.monthsMenu objectAtIndex:indexPath.row] isEqualToString:@"March"])
        self.eventNavLabel = @"March";
    else if ([[self.monthsMenu objectAtIndex:indexPath.row] isEqualToString:@"April"])
        self.eventNavLabel = @"April";
    else if ([[self.monthsMenu objectAtIndex:indexPath.row] isEqualToString:@"May"])
        self.eventNavLabel = @"May";
    [self performSegueWithIdentifier:@"EventsSegue" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"EventsSegue"]) {
        EventsViewController *eventsViewController = [segue destinationViewController];
        
        [eventsViewController.navLabel setTitle:self.eventNavLabel];
    }
}




#pragma mark - Table View Delegate Methods

/*- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //   We use the modulus operator % to find the remainder of the division of row number by 2.
    //   If the remainder is 0, it is an even number (use MintCream); otherwise, it is an odd number (use OldLace).
    
    if (indexPath.row % 2 == 0) {
        // Set even numbered row background color to MintCream, #F5FFFA  245,255,250
        cell.backgroundColor = LIGHT_BLUE;
    } else {
        // Set odd numbered row background color to OldLace, #FDF5E6   253,245,230
        cell.backgroundColor = LIGHT_PURPLE;
    }
}*/


//       Asks the table view delegate to return the height of a given row.
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kTableViewRowHeight;
}

@end
