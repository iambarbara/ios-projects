//
//  EventsViewController.m
//  VTSopa
//
//  Created by Barbara Brown on 10/30/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "EventsViewController.h"
#import "EventDetailViewController.h"

@interface EventsViewController ()
@end

@implementation EventsViewController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    
    CGRect frame = CGRectMake(0, 0, 75, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:20]];
    label.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView = label;
    label.text = self.navLabel.title;
    
    NSString *path;
    
    if ([self.navLabel.title isEqualToString:@"September"])
        path = [[NSBundle mainBundle] pathForResource:@"SepEvents" ofType:@"plist"];
    else if ([self.navLabel.title isEqualToString:@"October"])
        path = [[NSBundle mainBundle] pathForResource:@"OctEvents" ofType:@"plist"];
    else if ([self.navLabel.title isEqualToString:@"November"])
        path = [[NSBundle mainBundle] pathForResource:@"NovEvents" ofType:@"plist"];
    else if ([self.navLabel.title isEqualToString:@"December"])
        path = [[NSBundle mainBundle] pathForResource:@"DecEvents" ofType:@"plist"];
    else if ([self.navLabel.title isEqualToString:@"January"])
        path = [[NSBundle mainBundle] pathForResource:@"JanEvents" ofType:@"plist"];
    else if ([self.navLabel.title isEqualToString:@"February"])
        path = [[NSBundle mainBundle] pathForResource:@"FebEvents" ofType:@"plist"];
    else if ([self.navLabel.title isEqualToString:@"March"])
        path = [[NSBundle mainBundle] pathForResource:@"MarEvents" ofType:@"plist"];
    else if ([self.navLabel.title isEqualToString:@"April"])
        path = [[NSBundle mainBundle] pathForResource:@"AprEvents" ofType:@"plist"];
    else if ([self.navLabel.title isEqualToString:@"May"])
        path = [[NSBundle mainBundle] pathForResource:@"MayEvents" ofType:@"plist"];

    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    self.eventsType = dict;
    NSArray *array = [[_eventsType allKeys] sortedArrayUsingSelector:@selector(compare:)];
    self.eventName = array;
    
    [super viewDidLoad];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark Table View Data Source Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [self.eventName count];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSString *key = [self.eventName objectAtIndex:section];
    NSArray *name = [self.eventsType objectForKey:key];
    
    return [name count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EventCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventCellType"];
    
    NSString *key = [self.eventName objectAtIndex:indexPath.section];
    NSArray *name = [self.eventsType objectForKey:key];
    
    cell.eventLabel.text = [name objectAtIndex:indexPath.row];
    
    if ([key isEqualToString:@"Mainstage Theatre"])
        cell.backgroundColor = [UIColor colorWithRed:231.0f/255.0f green:224.0f/255.0f blue:231.0f/255.0f alpha:1.0f];
    else if ([key isEqualToString:@"Special Events"])
        cell.backgroundColor = [UIColor colorWithRed:240.0f/255.0f green:249.0f/255.0f blue:253.0f/255.0f alpha:1.0f];
    else if ([key isEqualToString:@"Cinema Events"])
        cell.backgroundColor = [UIColor colorWithRed:254.0f/255.0f green:240.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
    else if ([key isEqualToString:@"Music On Mondays"])
        cell.backgroundColor = [UIColor colorWithRed:253.0f/255.0f green:251.0f/255.0f blue:240.0f/255.0f alpha:1.0f];
    else if ([key isEqualToString:@"Visiting Artist Series"])
        cell.backgroundColor = [UIColor colorWithRed:248.0f/255.0f green:231.0f/255.0f blue:233.0f/255.0f alpha:1.0f];
    else if ([key isEqualToString:@"Ensemble Performance Series"])
        cell.backgroundColor = [UIColor colorWithRed:226.0f/255.0f green:243.0f/255.0f blue:244.0f/255.0f alpha:1.0f];
    
    return cell;
    
}
# pragma mark - Header View Methods

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *key = [self.eventName objectAtIndex:section];
    UIView *customTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 50)];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 300, 50)];
    titleLabel.text = key;
    [titleLabel setFont:[UIFont fontWithName:@"BlackJack" size:27]];
    
    if ([key isEqualToString:@"Mainstage Theatre"])
        titleLabel.textColor = [UIColor colorWithRed:121.0f/255.0f green:80.0f/255.0f blue:126.0f/255.0f alpha:1.0f];
    else if ([key isEqualToString:@"Special Events"])
        titleLabel.textColor = [UIColor colorWithRed:107.0f/255.0f green:207.0f/255.0f blue:246.0f/255.0f alpha:1.0f];
    else if ([key isEqualToString:@"Cinema Events"])
        titleLabel.textColor = [UIColor colorWithRed:240.0f/255.0f green:112.0f/255.0f blue:74.0f/255.0f alpha:1.0f];
    else if ([key isEqualToString:@"Music On Mondays"])
        titleLabel.textColor = [UIColor colorWithRed:196.0f/255.0f green:184.0f/255.0f blue:106.0f/255.0f alpha:1.0f];
    else if ([key isEqualToString:@"Visiting Artist Series"])
        titleLabel.textColor = [UIColor colorWithRed:217.0f/255.0f green:88.0f/255.0f blue:143.0f/255.0f alpha:1.0f];
    else if ([key isEqualToString:@"Ensemble Performance Series"])
        titleLabel.textColor = [UIColor colorWithRed:52.0f/255.0f green:187.0f/255.0f blue:191.0f/255.0f alpha:1.0f];
    
    titleLabel.backgroundColor = [UIColor clearColor];
    [customTitleView addSubview:titleLabel];
    
    return customTitleView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    NSString *key = [self.eventName objectAtIndex:section];
    return key;
    
}

#pragma mark - Table View Delegate Methods

//       Asks the table view delegate to return the height of a given row.
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kTableViewRowHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [self.eventName objectAtIndex:indexPath.section];
    NSArray *name = [self.eventsType objectForKey:key];
    self.name = [name objectAtIndex:indexPath.row];
    if ([key isEqualToString:@"Mainstage Theatre"])
        self.bgColor = [UIColor colorWithRed:231.0f/255.0f green:224.0f/255.0f blue:231.0f/255.0f alpha:1.0f];
    else if ([key isEqualToString:@"Special Events"])
        self.bgColor = [UIColor colorWithRed:240.0f/255.0f green:249.0f/255.0f blue:253.0f/255.0f alpha:1.0f];
    else if ([key isEqualToString:@"Cinema Events"])
        self.bgColor = [UIColor colorWithRed:254.0f/255.0f green:240.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
    else if ([key isEqualToString:@"Music On Mondays"])
        self.bgColor = [UIColor colorWithRed:253.0f/255.0f green:251.0f/255.0f blue:240.0f/255.0f alpha:1.0f];
    else if ([key isEqualToString:@"Visiting Artist Series"])
        self.bgColor = [UIColor colorWithRed:248.0f/255.0f green:231.0f/255.0f blue:233.0f/255.0f alpha:1.0f];
    else if ([key isEqualToString:@"Ensemble Performance Series"])
        self.bgColor = [UIColor colorWithRed:226.0f/255.0f green:243.0f/255.0f blue:244.0f/255.0f alpha:1.0f];

    [self performSegueWithIdentifier:@"EventDetailSegue" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    EventDetailViewController *eventDetailViewController = [segue destinationViewController];
    eventDetailViewController.view.backgroundColor = self.bgColor;
    eventDetailViewController.hidesBottomBarWhenPushed = YES;
    eventDetailViewController.eventName = self.name;
}


@end