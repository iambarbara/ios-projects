//
//  ContactWebViewController.h
//  VTSopa
//
//  Created by Barbara Brown on 12/1/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactWebViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSString *websiteURL;
@property (strong, nonatomic) NSString *navTitle;

@end
