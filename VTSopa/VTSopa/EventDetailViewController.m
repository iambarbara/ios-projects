//
//  EventDetailViewController.m
//  VTSopa
//
//  Created by Barbara Brown on 10/31/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "EventDetailViewController.h"
#import "EventsViewController.h"
//#import "EventDetailCell.h"
#import "EventCell.h"

@interface EventDetailViewController ()
@end

@implementation EventDetailViewController

- (void)viewDidLoad
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"EventDetails" ofType:@"plist"];
    self.eventList = [[NSArray alloc] initWithObjects:@"Event", @"Date & Time", @"Location", @"About", @"Price", nil];

    // Begin Event Detail Section
    self.eventDict = [[NSDictionary alloc] initWithContentsOfFile:path];
    //self.eventDict= dict;
    self.event = [[self.eventDict allKeys] sortedArrayUsingSelector:@selector(compare:)];
    //self.event = array;
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Section header to test against.
    NSString *key = [self.eventList objectAtIndex:indexPath.section];
    
    // Initialize Cell
    EventCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventInfo"];
    
    for (NSString *eName in self.event){
        if ([eName isEqualToString:self.eventName]){

            // Array of info for event
            NSArray *events = [self.eventDict objectForKey:eName];
            
            if ([key isEqualToString:@"Event"]){
                cell.eventLabel.text = eName;
                return cell;
            }
            if ([key isEqualToString:@"Date & Time"]){
                [cell.eventLabel setText:[events objectAtIndex:0]];
                return cell;
            }
            if ([key isEqualToString:@"Location"]){
                [cell.eventLabel setText:[events objectAtIndex:1]];
                return cell;
            }
            if ([key isEqualToString:@"About"]){
                [cell.eventLabel setText:[events objectAtIndex:2]];
                return cell;
            }
            if ([key isEqualToString:@"Price"]){
                [cell.eventLabel setText:[events objectAtIndex:3]];
                return cell;
            }
        }
    }

    return NULL;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [self.eventList count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}

# pragma mark - Header View Methods

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *key = [self.eventList objectAtIndex:section];
    UIView *customTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 30)];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 300, 30)];
    titleLabel.text = key;
    [titleLabel setFont:[UIFont fontWithName:@"BlackJack" size:20]];
    titleLabel.backgroundColor = [UIColor clearColor];
    [customTitleView addSubview:titleLabel];
    
    return customTitleView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    NSString *key = [self.eventList objectAtIndex:section];
    return key;
}


//       Asks the table view delegate to return the height of a given row.
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [self.eventList objectAtIndex:indexPath.section];
    
    // Initialize Cell
    EventCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventInfo"];
    
    for (NSString *eName in self.event){
        if ([eName isEqualToString:self.eventName]){
            
            // Array of info for event
            NSArray *events = [self.eventDict objectForKey:eName];
            
            if ([key isEqualToString:@"Event"])
                cell.eventLabel.text = eName;
            if ([key isEqualToString:@"Date & Time"])
                [cell.eventLabel setText:[events objectAtIndex:0]];
            if ([key isEqualToString:@"Location"])
                [cell.eventLabel setText:[events objectAtIndex:1]];
            if ([key isEqualToString:@"About"])
                [cell.eventLabel setText:[events objectAtIndex:2]];
            if ([key isEqualToString:@"Price"])
                [cell.eventLabel setText:[events objectAtIndex:3]];
        }
    }
    CGFloat cellHeight = cell.eventLabel.intrinsicContentSize.height;
    if (cellHeight <= 17)
        return 44;
    else
        return cell.eventLabel.intrinsicContentSize.height + 27;
}

@end
