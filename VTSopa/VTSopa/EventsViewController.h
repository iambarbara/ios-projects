//
//  EventsViewController.h
//  VTSopa
//
//  Created by Barbara Brown on 10/30/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventCell.h"

#define kTableViewRowHeight 60;

@interface EventsViewController : UITableViewController

@property (nonatomic, strong) NSDictionary *eventsType;
@property (nonatomic, strong) NSArray *eventName;
@property (strong, nonatomic) IBOutlet UINavigationItem *navLabel;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) UIColor *bgColor;
@end
