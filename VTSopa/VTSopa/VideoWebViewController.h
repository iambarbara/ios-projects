//
//  VideoWebViewController.h
//  VTSopa
//
//  Created by Barbara Brown on 12/11/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoWebViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSString *websiteURL;
@property (strong, nonatomic) NSString *navTitle;

@end
