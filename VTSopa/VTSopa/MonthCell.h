//
//  EventCell.h
//  VTSopa
//
//  Created by Barbara Brown on 10/30/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MonthCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *monthImage;
@property (nonatomic, strong) IBOutlet UILabel *monthLabel;
@end
