//
//  TicketsViewController.m
//  VTSopa
//
//  Created by Barbara Brown on 12/11/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "TicketsViewController.h"
#import "TicketsWebViewController.h"

@interface TicketsViewController ()

@property (strong, nonatomic) NSString *websiteURL;
@property (strong, nonatomic) NSString *navTitle;

@end

@implementation TicketsViewController

- (void)viewDidLoad
{
    [self.navigationController setNavigationBarHidden:YES];
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didTapPurchase:(id)sender {
    self.websiteURL = @"http://m.studentcenters.vt.edu/tickets/events_and_tickets/index.html";
    self.navTitle = @"Ticket Office";
    [self performSegueWithIdentifier:@"TicketWeb" sender:self];
}


#pragma mark - Preparing for Segue

// This method is called by the system whenever you invoke the method performSegueWithIdentifier:sender:
// You never call this method. It is invoked by the system.
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    TicketsWebViewController *ticketWeb = [segue destinationViewController];
    //if (sender isEqualToString:@"Facebook")
    ticketWeb.websiteURL = self.websiteURL;
    ticketWeb.navTitle = self.navTitle;
    ticketWeb.hidesBottomBarWhenPushed = YES;
}

@end
