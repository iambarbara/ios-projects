//
//  DataViewController.h
//  Test
//
//  Created by Barbara Brown on 12/1/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutDataViewController : UIViewController

@property (strong, nonatomic) id dataObject;
@property (strong, nonatomic) IBOutlet UITextView *textView;

@end
