//
//  AppDelegate.h
//  VTSopa
//
//  Created by Barbara Brown on 10/30/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, assign) BOOL fullScreenVideoIsPlaying;

@end
