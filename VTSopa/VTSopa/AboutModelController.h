//
//  ModelController.h
//  Test
//
//  Created by Barbara Brown on 12/1/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AboutDataViewController;

@interface AboutModelController : NSObject <UIPageViewControllerDataSource>

- (AboutDataViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard;
- (NSUInteger)indexOfViewController:(AboutDataViewController *)viewController;

@end
