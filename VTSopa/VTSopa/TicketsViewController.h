//
//  TicketsViewController.h
//  VTSopa
//
//  Created by Barbara Brown on 12/11/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *purchaseButton;
- (IBAction)didTapPurchase:(id)sender;

@end
