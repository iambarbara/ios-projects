//
//  ContactViewController.h
//  VTSopa
//
//  Created by Barbara Brown on 12/1/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *facebookButton;
@property (strong, nonatomic) IBOutlet UIButton *twitterButton;
@property (strong, nonatomic) IBOutlet UIButton *websiteButton;

- (IBAction)didTapFacebook:(id)sender;
- (IBAction)didTapTwitter:(id)sender;
- (IBAction)didTapWebsite:(id)sender;

@end
