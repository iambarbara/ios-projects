//
//  ContactViewController.m
//  VTSopa
//
//  Created by Barbara Brown on 12/1/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "ContactViewController.h"
#import "ContactWebViewController.h"

@interface ContactViewController ()

@property (strong, nonatomic) NSString *websiteURL;
@property (strong, nonatomic) NSString *navTitle;

@end

@implementation ContactViewController

- (void)viewDidLoad
{
    [self.navigationController setNavigationBarHidden:YES];
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didTapFacebook:(id)sender {
    self.websiteURL = @"http://www.facebook.com/VTperformingarts";
        self.navTitle = @"Facebook";
    [self performSegueWithIdentifier:@"ContactWeb" sender:self];
}

- (IBAction)didTapTwitter:(id)sender {
    self.websiteURL = @"http://www.twitter.com/vtsopa";
    self.navTitle = @"Twitter";
    [self performSegueWithIdentifier:@"ContactWeb" sender:self];
}

- (IBAction)didTapWebsite:(id)sender {
    self.websiteURL = @"http://www.sopac.vt.edu";
    self.navTitle = @"Website";
    [self performSegueWithIdentifier:@"ContactWeb" sender:self];
}


#pragma mark - Preparing for Segue

// This method is called by the system whenever you invoke the method performSegueWithIdentifier:sender:
// You never call this method. It is invoked by the system.
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ContactWebViewController *contactWeb = [segue destinationViewController];
    //if (sender isEqualToString:@"Facebook")
    contactWeb.websiteURL = self.websiteURL;
    contactWeb.navTitle = self.navTitle;
    contactWeb.hidesBottomBarWhenPushed = YES;
}

- (void) viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES];
}

@end
