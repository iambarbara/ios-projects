//
//  EventCell.h
//  VTSopa
//
//  Created by Barbara Brown on 10/31/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *eventLabel;
@property (strong, nonatomic) IBOutlet UILabel *videoLabel;
@property (strong, nonatomic) IBOutlet UIImageView *thumbnailImage;

@end
