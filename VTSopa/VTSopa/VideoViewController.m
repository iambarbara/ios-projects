//
//  VideoViewController.m
//  VTSopa
//
//  Created by Barbara Brown on 11/30/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "VideoViewController.h"
#import "EventCell.h"
#import "VideoWebViewController.h"

@interface VideoViewController ()
@property (strong, nonatomic) NSString *websiteURL;
@property (strong, nonatomic) NSString *navTitle;
@end

@implementation VideoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Videos";
    CGRect frame = CGRectMake(0, 0, 75, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:20]];
    label.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView = label;
    label.text = self.navigationItem.title;
    
    // Get information from plist file
    // **************************************************************
    NSString *path = [[NSBundle mainBundle] pathForResource:@"videos" ofType:@"plist"];
    self.videoDict = [[NSDictionary alloc] initWithContentsOfFile:path];
    self.videoList = [[self.videoDict allKeys] sortedArrayUsingSelector:@selector(compare:)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSString *key = [self.videoList objectAtIndex:indexPath.row];
    NSArray *data = [self.videoDict objectForKey:key];
    
    EventCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VideoCell"];
    
    cell.videoLabel.text = key;
    cell.thumbnailImage.image = [UIImage imageNamed:data[1]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = [self.videoList objectAtIndex:indexPath.row];
    NSArray *data = [self.videoDict objectForKey:key];
    self.websiteURL = data[0];
    self.navTitle = key;
    [self performSegueWithIdentifier:@"VideoWeb" sender:self];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.videoList count];
}


#pragma mark - Preparing for Segue

// This method is called by the system whenever you invoke the method performSegueWithIdentifier:sender:
// You never call this method. It is invoked by the system.
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    VideoWebViewController *videoWeb = [segue destinationViewController];
    videoWeb.navTitle = self.navTitle;
    videoWeb.websiteURL = self.websiteURL;
    videoWeb.hidesBottomBarWhenPushed = YES;
}

@end