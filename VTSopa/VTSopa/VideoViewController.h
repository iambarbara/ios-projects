//
//  VideoViewController.h
//  VTSopa
//
//  Created by Barbara Brown on 11/30/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoViewController : UITableViewController
@property (nonatomic, strong) NSDictionary *videoDict;
@property (nonatomic, strong) NSArray *videoList;

@property (strong, nonatomic) NSString *videoURL;
@end
