//
//  EventsViewController.h
//  VTSopa
//
//  Created by Barbara Brown on 10/30/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kTableViewRowHeight 60;

// Define MintCream color: #F5FFFA  245,255,250
#define LIGHT_BLUE [UIColor colorWithRed:253.0f/255.0f green:251.0f/255.0f blue:240.0f/255.0f alpha:1.0f];

// Define OldLace color: #FDF5E6   253,245,230
#define LIGHT_PURPLE [UIColor colorWithRed:231.0f/255.0f green:224.0f/255.0f blue:231.0f/255.0f alpha:1.0f];

@interface MonthsViewController : UITableViewController
//@property (strong, nonatomic) IBOutlet UITableView *monthTableView;
@property (strong, nonatomic) NSArray *monthsMenu;

@end
