//
//  EventDetailViewController.h
//  VTSopa
//
//  Created by Barbara Brown on 10/31/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventDetailViewController : UITableViewController
@property (nonatomic, strong) NSString *eventName;
//@property (strong, nonatomic) NSString *buyTickets;

@property (nonatomic, strong) UILabel *testLabel;

@property (nonatomic, strong) NSDictionary *eventDict;
@property (nonatomic, strong) NSArray *event;
@property (nonatomic, strong) NSArray *eventList;
@end
