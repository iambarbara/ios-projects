//
//  PriorityViewController.m
//  MyToDoList
//
//  Created by Barbara Brown on 12/10/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "PriorityViewController.h"

@interface PriorityViewController ()

@property (nonatomic, strong) NSDictionary *toDoDict;
@property (nonatomic, strong) NSArray *toDoArray;
@property (nonatomic, strong) NSMutableDictionary *sortedDict;
@property (nonatomic, strong) NSArray *sortedArray;
@property (nonatomic, strong) NSMutableDictionary *low;
@property (nonatomic, strong) NSMutableDictionary *normal;
@property (nonatomic, strong) NSMutableDictionary *high;
@property (nonatomic, strong) NSArray *lowArray;
@property (nonatomic, strong) NSArray *normalArray;
@property (nonatomic, strong) NSArray *highArray;

@property (nonatomic, strong) NSString *segueTitle;
@property (nonatomic, strong) NSArray *segueDetailArray;

@end

@implementation PriorityViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    // Obtain an object reference to the App Delegate object
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // Set the local instance variable to the obj ref of the favouriteMovies dictionary
    // data structure created in the App Delegate class
    self.toDoDict = appDelegate.toDoList;
    
    self.low = [[NSMutableDictionary alloc]init];
    self.normal = [[NSMutableDictionary alloc]init];
    self.high = [[NSMutableDictionary alloc]init];
    
    // Obtain a sorted list of movie genres and store them in a static array
    NSArray *toDoStaticArray = [[self.toDoDict allKeys] sortedArrayUsingSelector:@selector(compare:)];
    NSMutableArray *toDoMutable = [[NSMutableArray alloc] initWithArray:toDoStaticArray];
    //self.sortedArray = [[NSMutableArray alloc] initWithArray:toDoStaticArray];
    
    // Get all information for low priority events
    // *********************************************************************
    for (int j=0; j<toDoStaticArray.count; j++)
    {
        for (int k=0; k<toDoMutable.count; k++)
        {
            NSString *newCode = [toDoMutable objectAtIndex:k];
            NSArray *newArray = [self.toDoDict objectForKey:newCode];

            //NSComparisonResult result = [minCode compare:newCode];
            if ([newArray[2] isEqualToString:@"Low"])
                [self.low setObject:newArray forKey:newCode];
        }
    }
    // *********************************************************************
    
    // Get all information for normal priority events
    // *********************************************************************
    toDoMutable = [[NSMutableArray alloc] initWithArray:toDoStaticArray];
    for (int j=0; j<toDoStaticArray.count; j++)
    {
        for (int k=0; k<toDoMutable.count; k++)
        {
            NSString *newCode = [toDoMutable objectAtIndex:k];
            NSArray *newArray = [self.toDoDict objectForKey:newCode];
            
            //NSComparisonResult result = [minCode compare:newCode];
            if ([newArray[2] isEqualToString:@"Normal"])
                [self.normal setObject:newArray forKey:newCode];
        }
    }
    // *********************************************************************
    
    // Get all information for high priority events
    // *********************************************************************
    toDoMutable = [[NSMutableArray alloc] initWithArray:toDoStaticArray];
    for (int j=0; j<toDoStaticArray.count; j++)
    {
        for (int k=0; k<toDoMutable.count; k++)
        {
            NSString *newCode = [toDoMutable objectAtIndex:k];
            NSArray *newArray = [self.toDoDict objectForKey:newCode];
            
            //NSComparisonResult result = [minCode compare:newCode];
            if ([newArray[2] isEqualToString:@"High"])
                [self.high setObject:newArray forKey:newCode];
        }
    }
    // *********************************************************************
    
    self.lowArray = [[self.low allKeys] sortedArrayUsingSelector:@selector(compare:)];
    self.normalArray = [[self.normal allKeys] sortedArrayUsingSelector:@selector(compare:)];
    self.highArray = [[self.high allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
    self.toDoArray = [[NSMutableArray alloc] initWithArray:self.highArray];
    self.toDoArray = [self.toDoArray arrayByAddingObjectsFromArray:self.normalArray];
    self.toDoArray = [self.toDoArray arrayByAddingObjectsFromArray:self.lowArray];
    
    self.sortedDict = [[NSMutableDictionary alloc] init];
    NSString *alphabetize = @"";
    for (int i=0; i<self.highArray.count; i++)
    {
        alphabetize = [alphabetize stringByAppendingString:@"a"];
        [self.sortedDict setObject:[self.high objectForKey:self.highArray[i]] forKey:alphabetize];
    }
    for (int i=0; i<self.normalArray.count; i++)
    {
        alphabetize = [alphabetize stringByAppendingString:@"a"];
        [self.sortedDict setObject:[self.normal objectForKey:self.normalArray[i]] forKey:alphabetize];
    }
    for (int i=0; i<self.lowArray.count; i++)
    {
        alphabetize = [alphabetize stringByAppendingString:@"a"];
        [self.sortedDict setObject:[self.low objectForKey:self.lowArray[i]] forKey:alphabetize];
    }

    
    self.toDoDict = self.sortedDict;
    self.sortedArray = [[self.toDoDict allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
    [self.toDoTableView reloadData];
    [super viewDidAppear:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource Protocol Methods

/*
 We are implementing a Grouped table view style. In the storyboard file,
 select the Table View. Under the Attributes Inspector, set the Style attribute to Grouped.
 */

// Each table view section corresponds to a country
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

// Asks the data source to return the number of rows in a given section.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.sortedArray count];
}

// Asks the data source to return a cell to insert in a particular table view location
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *toDoCode = [self.sortedArray objectAtIndex:indexPath.row];
    NSArray *toDoData = [self.toDoDict objectForKey:toDoCode];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ToDoCell"];
    
    cell.textLabel.text = [self.toDoArray objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = toDoData[3];
    
    if ([toDoData[0] isEqualToString:@"YES"])
        cell.imageView.image = [UIImage imageNamed:@"checkedBox"];
    else if ([toDoData[0] isEqualToString:@"NO"])
        cell.imageView.image = [UIImage imageNamed:@"checkBox"];
    
    if ([toDoData[2] isEqualToString:@"Low"])
        cell.textLabel.textColor = [UIColor blackColor];
    else if ([toDoData[2] isEqualToString:@"Normal"])
        cell.textLabel.textColor = [UIColor brownColor];
    else if ([toDoData[2] isEqualToString:@"High"])
        cell.textLabel.textColor = [UIColor redColor];
    
    return cell;
}

#pragma mark - UITableViewDelegate Protocol Methods

// Tapping a row displays an view controller with detailed information
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.segueTitle = [self.toDoArray objectAtIndex:indexPath.row];
    NSString *title = [self.sortedArray objectAtIndex:indexPath.row];
    self.segueDetailArray = [self.sortedDict objectForKey:title];
    
    [self performSegueWithIdentifier:@"ToDoDetail" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Preparing for Segue

// This method is called by the system whenever you invoke the method performSegueWithIdentifier:sender:
// You never call this method. It is invoked by the system.
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString *segueIdentifier = [segue identifier];
    
    if ([segueIdentifier isEqualToString:@"ToDoDetail"]) {
        // Obtain the object reference of the destination view controller
        ToDoDetailViewController *detailViewController = [segue destinationViewController];
        detailViewController.segueDetailArray = self.segueDetailArray;
        detailViewController.segueTitle = self.segueTitle;
    }
}

@end
