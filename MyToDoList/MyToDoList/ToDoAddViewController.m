//
//  ToDoAddViewController.m
//  MyToDoList
//
//  Created by Barbara Brown on 12/10/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "ToDoAddViewController.h"

@interface ToDoAddViewController ()

@end

@implementation ToDoAddViewController

- (void)viewDidLoad
{
    self.navigationController.title = @"Add";
    
    // Deselcted UISegmentedControl selections
    [self.toDoPriority setSelectedSegmentIndex:UISegmentedControlNoSegment];
    [self.toDoCompleted setSelectedSegmentIndex:UISegmentedControlNoSegment];
    
    self.toDoDescription.text = @"";
    
    // Instantiate a Save button to invoke the save: method when tapped
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc]
                                   initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                   target:self action:@selector(save:)];
    
    // Set up the Save custom button on the right of the navigation bar
    self.navigationItem.rightBarButtonItem = saveButton;
    
}

- (IBAction)keyboardDone:(id)sender
{
    [sender resignFirstResponder];  // Deactivate the keyboard
}

// This method is invoked when the user taps anywhere on the background
- (IBAction)backgroundTouch:(UIControl *)sender {
    
    // Deactivate the UITextField objects and remove the Keyboard
    [self.toDoTitle resignFirstResponder];
    [self.toDoDescription resignFirstResponder];
}


- (void)save:(id)sender
{
    if ([self.toDoTitle.text isEqualToString:@""])
    {
        // The user attempts to move a city from one country to another, which is prohibited
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Title Empty"
                              message:@"Please enter a title!"
                              delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
        return;
    }
    else if ([self.toDoDescription.text isEqualToString:@""])
    {
        // The user attempts to move a city from one country to another, which is prohibited
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Description Empty"
                              message:@"Please enter a description!"
                              delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
        return;
    }
    else if (self.toDoPriority.selectedSegmentIndex == UISegmentedControlNoSegment)
    {
        // Create and display an error message
        UIAlertView *alertMessage = [[UIAlertView alloc] initWithTitle:@"Selection Missing"
                                                               message:@"Please select a priority!"
                                                              delegate:nil
                                                     cancelButtonTitle:@"Okay"
                                                     otherButtonTitles:nil];
        
        [alertMessage show];
        return;
    }
    else if (self.toDoCompleted.selectedSegmentIndex == UISegmentedControlNoSegment)
    {
        // Create and display an error message
        UIAlertView *alertMessage = [[UIAlertView alloc] initWithTitle:@"Selection Missing"
                                                               message:@"Please select YES or NO for 'To Do Complete'!"
                                                              delegate:nil
                                                     cancelButtonTitle:@"Okay"
                                                     otherButtonTitles:nil];
        
        [alertMessage show];
        return;
    }
    // Inform the delegate that the user tapped the Save button
    [self.delegate toDoAddViewController:self didFinishWithSave:YES];
}


@end
