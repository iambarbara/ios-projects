//
//  ToDoDetailViewController.m
//  MyToDoList
//
//  Created by Barbara Brown on 12/10/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "ToDoDetailViewController.h"

@interface ToDoDetailViewController ()
@end

@implementation ToDoDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.toDoTitle.text = self.segueTitle;
    self.toDoCompleted.text = self.segueDetailArray[0];
    self.toDoDescription.text = self.segueDetailArray[1];
    self.toDoPriority.text = self.segueDetailArray[2];
    self.toDoTime.text = self.segueDetailArray[3];
    [self.toDoDescription setFont:[UIFont systemFontOfSize:17]];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
