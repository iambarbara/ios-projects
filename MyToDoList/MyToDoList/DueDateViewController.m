//
//  DueDateViewController.m
//  MyToDoList
//
//  Created by Barbara Brown on 12/10/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "DueDateViewController.h"

@interface DueDateViewController ()

@property (nonatomic, strong) NSDictionary *toDoDict;
@property (nonatomic, strong) NSArray *toDoArray;
@property (nonatomic, strong) NSMutableDictionary *sortedDict;
@property (nonatomic, strong) NSMutableArray *sortedArray;

@property (nonatomic, strong) NSString *segueTitle;
@property (nonatomic, strong) NSArray *segueDetailArray;

@end

@implementation DueDateViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    // Obtain an object reference to the App Delegate object
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // Set the local instance variable to the obj ref of the favouriteMovies dictionary
    // data structure created in the App Delegate class
    self.toDoDict = appDelegate.toDoList;
    
    // Obtain a sorted list of movie genres and store them in a static array
    NSArray *toDoStaticArray = [[self.toDoDict allKeys] sortedArrayUsingSelector:@selector(compare:)];
    NSMutableArray *toDoMutable = [[NSMutableArray alloc] initWithArray:toDoStaticArray];
    
    
    self.sortedDict = [[NSMutableDictionary alloc] init];
    self.sortedArray = [[NSMutableArray alloc] initWithCapacity:toDoMutable.count];

    NSString *newKey = @"";
    for (int j=0; j<toDoStaticArray.count; j++)
    {
        NSString *minCode = [toDoMutable objectAtIndex:0];
        NSArray *minArray = [self.toDoDict objectForKey:minCode];
        //******************************************************************************
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd 'at' HH:mm"];
        NSDate *minDate = [dateFormatter dateFromString:minArray[3]];
        //******************************************************************************
        
        for (int k=0; k<toDoMutable.count; k++)
        {
            NSString *newCode = [toDoMutable objectAtIndex:k];
            NSArray *newArray = [self.toDoDict objectForKey:newCode];
            NSDate *newDate = [dateFormatter dateFromString:newArray[3]];

            minDate = [newDate earlierDate:minDate];
            if ([newDate isEqualToDate:minDate])
            {
                minArray = newArray;
                minCode = newCode;
            }
        }
        newKey = [newKey stringByAppendingString:@"a"];
        self.sortedArray[j] = minCode;
        [self.sortedDict setObject:minArray forKey:newKey];
        [toDoMutable removeObject:minCode];
    }
    
    self.toDoDict = [[NSDictionary alloc] initWithDictionary:self.sortedDict];
    self.toDoArray = [[self.toDoDict allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
    [self.toDoTableView reloadData];
    [super viewDidAppear:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource Protocol Methods

/*
 We are implementing a Grouped table view style. In the storyboard file,
 select the Table View. Under the Attributes Inspector, set the Style attribute to Grouped.
 */

// Each table view section corresponds to a country
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

// Asks the data source to return the number of rows in a given section.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.toDoArray count];
}

// Asks the data source to return a cell to insert in a particular table view location
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *toDoCode = [self.toDoArray objectAtIndex:indexPath.row];
    NSArray *toDoData = [self.toDoDict objectForKey:toDoCode];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ToDoCell"];
    
    cell.textLabel.text = [self.sortedArray objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = toDoData[3];
    
    if ([toDoData[0] isEqualToString:@"YES"])
        cell.imageView.image = [UIImage imageNamed:@"checkedBox"];
    else if ([toDoData[0] isEqualToString:@"NO"])
        cell.imageView.image = [UIImage imageNamed:@"checkBox"];
    
    if ([toDoData[2] isEqualToString:@"Low"])
        cell.textLabel.textColor = [UIColor blackColor];
    else if ([toDoData[2] isEqualToString:@"Normal"])
        cell.textLabel.textColor = [UIColor brownColor];
    else if ([toDoData[2] isEqualToString:@"High"])
        cell.textLabel.textColor = [UIColor redColor];
    
    return cell;
}

#pragma mark - UITableViewDelegate Protocol Methods

// Tapping a row displays an view controller with detailed information
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.segueTitle = [self.sortedArray objectAtIndex:indexPath.row];
    NSString *title = [self.toDoArray objectAtIndex:indexPath.row];
    self.segueDetailArray = [self.sortedDict objectForKey:title];
    
    [self performSegueWithIdentifier:@"ToDoDetail" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Preparing for Segue

// This method is called by the system whenever you invoke the method performSegueWithIdentifier:sender:
// You never call this method. It is invoked by the system.
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString *segueIdentifier = [segue identifier];
    
    if ([segueIdentifier isEqualToString:@"ToDoDetail"]) {
        // Obtain the object reference of the destination view controller
        ToDoDetailViewController *detailViewController = [segue destinationViewController];
        detailViewController.segueDetailArray = self.segueDetailArray;
        detailViewController.segueTitle = self.segueTitle;
    }
}

@end
