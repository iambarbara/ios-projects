//
//  ToDoDetailViewController.h
//  MyToDoList
//
//  Created by Barbara Brown on 12/10/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToDoDetailViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *toDoTitle;
@property (strong, nonatomic) IBOutlet UITextView *toDoDescription;
@property (strong, nonatomic) IBOutlet UILabel *toDoTime;
@property (strong, nonatomic) IBOutlet UILabel *toDoPriority;
@property (strong, nonatomic) IBOutlet UILabel *toDoCompleted;


@property (nonatomic, strong) NSString *segueTitle;
@property (nonatomic, strong) NSArray *segueDetailArray;

@end
