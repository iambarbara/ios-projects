//
//  ToDoEditViewController.m
//  MyToDoList
//
//  Created by Barbara Brown on 12/10/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "ToDoEditViewController.h"

@interface ToDoEditViewController ()
@end

@implementation ToDoEditViewController

- (void)viewDidLoad
{
    self.navigationController.title = @"Change";

    self.oldTitle = self.segueTitle;
    self.toDoTitle.text = self.segueTitle;
    self.toDoDescription.text = self.segueDetailArray[1];
    
    // Set UIDatePicker's date from previous information
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd 'at' HH:mm"];
    NSDate *date = [dateFormatter dateFromString:self.segueDetailArray[3]];
    [self.datePickerView setDate:date animated:YES];
    
    // Set UISegmentedControl selection for completed
    if ([self.segueDetailArray[0] isEqualToString:@"YES"])
        self.toDoCompleted.selectedSegmentIndex = 0;
    else if ([self.segueDetailArray[0] isEqualToString:@"NO"])
        self.toDoCompleted.selectedSegmentIndex = 1;
    
    // Set UISegmentedControl selection for priority
    if ([self.segueDetailArray[2] isEqualToString:@"Low"])
        self.toDoPriority.selectedSegmentIndex = 0;
    else if ([self.segueDetailArray[2] isEqualToString:@"Normal"])
        self.toDoPriority.selectedSegmentIndex = 1;
    else if ([self.segueDetailArray[2] isEqualToString:@"High"])
        self.toDoPriority.selectedSegmentIndex = 2;
        
    // Instantiate a Save button to invoke the save: method when tapped
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc]
                                   initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                   target:self action:@selector(save:)];
    
    // Set up the Save custom button on the right of the navigation bar
    self.navigationItem.rightBarButtonItem = saveButton;

}

- (IBAction)keyboardDone:(id)sender
{
    [sender resignFirstResponder];  // Deactivate the keyboard
}

// This method is invoked when the user taps anywhere on the background
- (IBAction)backgroundTouch:(UIControl *)sender {
    
    // Deactivate the UITextField objects and remove the Keyboard
    [self.toDoTitle resignFirstResponder];
    [self.toDoDescription resignFirstResponder];
}


- (void)save:(id)sender
{
    if ([self.toDoTitle.text isEqualToString:@""])
    {
        // The user attempts to move a city from one country to another, which is prohibited
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Title Empty"
                              message:@"Please enter a title!"
                              delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
        return;
    }
    else if ([self.toDoDescription.text isEqualToString:@""])
    {
        // The user attempts to move a city from one country to another, which is prohibited
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Description Empty"
                              message:@"Please enter a description!"
                              delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    
    
    // Inform the delegate that the user tapped the Save button
    [self.delegate toDoEditViewController:self didFinishWithSave:YES];
}


@end
