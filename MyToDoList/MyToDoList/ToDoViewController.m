//
//  ToDoViewController.m
//  MyToDoList
//
//  Created by Barbara Brown on 12/10/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "ToDoViewController.h"
#import "AppDelegate.h"
#import "ToDoDetailViewController.h"
#import "ToDoEditViewController.h"
#import "ToDoAddViewController.h"

@interface ToDoViewController ()

@property (nonatomic, strong) NSMutableDictionary *toDoDict;
@property (nonatomic, strong) NSMutableArray *toDoArray;

@property (nonatomic, strong) NSString *segueTitle;
@property (nonatomic, strong) NSArray *segueDetailArray;

// This method is invoked when the user taps the Add button created at run time.
- (void):(id)sender;


@end

@implementation ToDoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Obtain an object reference to the App Delegate object
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // Set the local instance variable to the obj ref of the favouriteMovies dictionary
    // data structure created in the App Delegate class
    self.toDoDict = appDelegate.toDoList;
    
    // Set up the Edit system button on the left of the navigation bar
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    /*
     editButtonItem is provided by the system with its own functionality. Tapping it triggers editing by
     displaying the red minus sign icon on all rows. Tapping the minus sign displays the Delete button.
     The Delete button is handled in the method tableView: commitEditingStyle: forRowAtIndexPath:
     */
    
    // Instantiate an Add button (with plus sign icon) to invoke the : method when tapped.
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc]
                                  initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                  target:self action:@selector(:)];
    
    // Set up the Add custom button on the right of the navigation bar
    self.navigationItem.rightBarButtonItem = addButton;

    // Obtain a sorted list of movie genres and store them in a static array
    NSArray *toDoStaticArray = [[self.toDoDict allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
    // Set the mutable sorted array to hold the movie genres in a mutable (changeable) array
    self.toDoArray = [[NSMutableArray alloc] initWithArray:toDoStaticArray];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Add City Method

// The : method is invoked when the user taps the Add button created at run time.
- (void):(id)sender
{
    // Perform the segue named
    [self performSegueWithIdentifier:@"ToDoAdd" sender:self];
}

#pragma mark - UITableViewDataSource Protocol Methods

/*
 We are implementing a Grouped table view style. In the storyboard file,
 select the Table View. Under the Attributes Inspector, set the Style attribute to Grouped.
 */

// Each table view section corresponds to a country
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

// Asks the data source to return the number of rows in a given section.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.toDoArray count];
}

// Asks the data source to return a cell to insert in a particular table view location
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *toDoCode = [self.toDoArray objectAtIndex:indexPath.row];
    NSArray *toDoData = [self.toDoDict objectForKey:toDoCode];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ToDoCell"];
    
    cell.textLabel.text = toDoCode;
    cell.detailTextLabel.text = toDoData[3];
    
    if ([toDoData[0] isEqualToString:@"YES"])
        cell.imageView.image = [UIImage imageNamed:@"checkedBox"];
    else if ([toDoData[0] isEqualToString:@"NO"])
        cell.imageView.image = [UIImage imageNamed:@"checkBox"];
    
    if ([toDoData[2] isEqualToString:@"Low"])
        cell.textLabel.textColor = [UIColor blackColor];
    else if ([toDoData[2] isEqualToString:@"Normal"])
        cell.textLabel.textColor = [UIColor brownColor];
    else if ([toDoData[2] isEqualToString:@"High"])
        cell.textLabel.textColor = [UIColor redColor];
    
    return cell;
}

#pragma mark - UITableViewDelegate Protocol Methods

// Tapping a row displays an view controller with detailed information
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.segueTitle = [self.toDoArray objectAtIndex:indexPath.row];
    self.segueDetailArray = [self.toDoDict objectForKey:self.segueTitle];
    
    [self performSegueWithIdentifier:@"ToDoDetail" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

// Informs the table view delegate that the user tapped the detail accessory button
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    self.segueTitle = [self.toDoArray objectAtIndex:indexPath.row];
    self.segueDetailArray = [self.toDoDict objectForKey:self.segueTitle];
    
    [self performSegueWithIdentifier:@"ToDoEdit" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

// We allow each row (city) of the table view to be editable, i.e., deletable or movable
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


// This is the method invoked when the user taps the Delete button in the Edit mode
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {  // Handle the Delete action
        
        NSString *toDelete = [self.toDoArray objectAtIndex:indexPath.row];
        [self.toDoArray removeObject:toDelete];
        [self.toDoDict removeObjectForKey:toDelete];

        // Reload the rows and sections of the Table View
        [self.toDoTableView reloadData];
        
    }
}

#pragma mark - ViewControllerDelegate Protocol Method

/*
 This is the ViewController's delegate method we created. ToDoEditViewController informs
 the delegate ToDoViewController that the user tapped the Save button if the parameter is YES.
 */
- (void)toDoEditViewController:(ToDoEditViewController *)controller didFinishWithSave:(BOOL)save
{
    if (save)
    {
        NSString *oldTitle = controller.oldTitle;
        //NSMutableArray *toDoData = [self.toDoDict objectForKey:title];
        
        // Gather the information to be changed form information entered
        // *************************************************************
        NSString *titleEntered = controller.toDoTitle.text;
        NSString *descriptionEntered = controller.toDoDescription.text;

        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd 'at' HH:mm"];
        NSDate *date =[controller.datePickerView date];
        NSString *dateEntered = [dateFormatter stringFromDate:date];
        
        NSString *priorityEntered = [[NSString alloc] init];
        switch ([controller.toDoPriority selectedSegmentIndex]) {
                
            case 0:   // Roadmap map type selected
                priorityEntered = @"Low";
                break;
                
            case 1:   // Satellite map type selected
                priorityEntered = @"Normal";
                break;
                
            case 2:   // Terrain map type selected
                priorityEntered = @"High";
                break;
        }
        
        
        NSString *completedEntered = [[NSString alloc] init];
        switch ([controller.toDoCompleted selectedSegmentIndex]) {
                
            case 0:   // Roadmap map type selected
                completedEntered = @"YES";
                break;
                
            case 1:   // Satellite map type selected
                completedEntered = @"NO";
                break;
        }
        // *************************************************************
        
        NSMutableArray *newData = [[NSMutableArray alloc] init];
        [newData insertObject:completedEntered atIndex:0];
        [newData insertObject:descriptionEntered atIndex:1];
        [newData insertObject:priorityEntered atIndex:2];
        [newData insertObject:dateEntered atIndex:3];
        
        // If the title is changed:
        if (![oldTitle isEqualToString:titleEntered])
        {
            [self.toDoDict setObject:newData forKey:titleEntered];
            // *********************************************************************
            // Obtain a sorted list of movie genres and store them in a static array
            NSArray *toDoStaticArray = [[self.toDoDict allKeys] sortedArrayUsingSelector:@selector(compare:)];
            
            // Set the mutable sorted array to hold the movie genres in a mutable (changeable) array
            self.toDoArray = [[NSMutableArray alloc] initWithArray:toDoStaticArray];
            // *********************************************************************
            
            [self.toDoArray removeObject:oldTitle];
        }
        
        // Otherwise, just change to do data
        [self.toDoDict setObject:newData forKey:oldTitle];
        
        // Reload TableView data
        [self.toDoTableView reloadData];
    }
    
    /*
     Pop the current view controller AddMovieViewController from the stack
     and show the next view controller in the stack, which is ViewController.
     */
    [self.navigationController popViewControllerAnimated:YES];
}


/*
 This is the ViewController's delegate method we created. ToDoAddViewController informs
 the delegate ToDoViewController that the user tapped the Save button if the parameter is YES.
 */
- (void)toDoAddViewController:(ToDoAddViewController *)controller didFinishWithSave:(BOOL)save
{
    if (save)
    {
       
        // Gather the information to be changed form information entered
        // *************************************************************
        NSString *titleEntered = controller.toDoTitle.text;
        NSString *descriptionEntered = controller.toDoDescription.text;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd 'at' HH:mm"];
        NSDate *date =[controller.datePickerView date];
        NSString *dateEntered = [dateFormatter stringFromDate:date];
        
        NSString *priorityEntered = [[NSString alloc] init];
        switch ([controller.toDoPriority selectedSegmentIndex]) {
                
            case 0:   // Roadmap map type selected
                priorityEntered = @"Low";
                break;
                
            case 1:   // Satellite map type selected
                priorityEntered = @"Normal";
                break;
                
            case 2:   // Terrain map type selected
                priorityEntered = @"High";
                break;
        }
        
        
        NSString *completedEntered = [[NSString alloc] init];
        switch ([controller.toDoCompleted selectedSegmentIndex]) {
                
            case 0:   // Roadmap map type selected
                completedEntered = @"YES";
                break;
                
            case 1:   // Satellite map type selected
                completedEntered = @"NO";
                break;
        }
        // *************************************************************
        
        NSMutableArray *newData = [[NSMutableArray alloc] init];
        [newData insertObject:completedEntered atIndex:0];
        [newData insertObject:descriptionEntered atIndex:1];
        [newData insertObject:priorityEntered atIndex:2];
        [newData insertObject:dateEntered atIndex:3];
        

        [self.toDoDict setObject:newData forKey:titleEntered];
        // *********************************************************************
        // Obtain a sorted list of movie genres and store them in a static array
        NSArray *toDoStaticArray = [[self.toDoDict allKeys] sortedArrayUsingSelector:@selector(compare:)];
        
        // Set the mutable sorted array to hold the movie genres in a mutable (changeable) array
        self.toDoArray = [[NSMutableArray alloc] initWithArray:toDoStaticArray];
        // *********************************************************************
        
        // Reload TableView data
        [self.toDoTableView reloadData];
    }
    
    /*
     Pop the current view controller AddMovieViewController from the stack
     and show the next view controller in the stack, which is ViewController.
     */
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Preparing for Segue

// This method is called by the system whenever you invoke the method performSegueWithIdentifier:sender:
// You never call this method. It is invoked by the system.
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString *segueIdentifier = [segue identifier];
    
    if ([segueIdentifier isEqualToString:@"ToDoDetail"]) {
        // Obtain the object reference of the destination view controller
        ToDoDetailViewController *detailViewController = [segue destinationViewController];
        detailViewController.segueDetailArray = self.segueDetailArray;
        detailViewController.segueTitle = self.segueTitle;
    }
    else if ([segueIdentifier isEqualToString:@"ToDoEdit"]) {
        // Obtain the object reference of the destination view controller
        ToDoEditViewController *editViewController = [segue destinationViewController];
        editViewController.segueDetailArray = self.segueDetailArray;
        editViewController.segueTitle = self.segueTitle;
        editViewController.delegate = self;
    }
    else if ([segueIdentifier isEqualToString:@"ToDoAdd"]) {
        // Obtain the object reference of the destination view controller
        ToDoEditViewController *addViewController = [segue destinationViewController];
        addViewController.delegate = self;
    }
}


@end
