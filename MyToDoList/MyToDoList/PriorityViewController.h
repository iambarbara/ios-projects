//
//  PriorityViewController.h
//  MyToDoList
//
//  Created by Barbara Brown on 12/10/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ToDoDetailViewController.h"

@interface PriorityViewController : UITableViewController

@property (nonatomic, strong) IBOutlet UITableView *toDoTableView;

@end
