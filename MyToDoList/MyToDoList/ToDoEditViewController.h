//
//  ToDoEditViewController.h
//  MyToDoList
//
//  Created by Barbara Brown on 12/10/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 We use the Delegation Design Pattern for the communication between the
 ToDoEditViewController object and the ToDoViewController Object.
 We define a protocol here and the ToDoViewController adopts it.
 */
@protocol ToDoEditViewControllerDelegate;

@interface ToDoEditViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *toDoTitle;
@property (strong, nonatomic) IBOutlet UITextView *toDoDescription;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePickerView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *toDoPriority;
@property (strong, nonatomic) IBOutlet UISegmentedControl *toDoCompleted;

@property (nonatomic, strong) NSString *oldTitle;
@property (nonatomic, strong) NSString *segueTitle;
@property (nonatomic, strong) NSArray *segueDetailArray;

@property (nonatomic, strong) NSString *priority;
@property (nonatomic, strong) NSString *completed;

@property (nonatomic, assign) id <ToDoEditViewControllerDelegate> delegate;

// The keyboardDone: method is invoked when the user taps Done on the keyboard
- (IBAction)keyboardDone:(id)sender;

// The save: method is invoked when the user taps the Save button created at run time.
- (void)save:(id)sender;

// This method is invoked when the user taps anywhere on the background
- (IBAction)backgroundTouch:(UIControl *)sender;

@end


/*
 The Protocol must be specified after the Interface specification is ended.
 Guidelines:
 - Create a protocol name as ClassNameDelegate as we did above.
 - Create a protocol method name starting with the name of the class defining the protocol.
 - Make the first method parameter to be the object reference of the caller as we did below.
 */
@protocol ToDoEditViewControllerDelegate

- (void)toDoEditViewController:(ToDoEditViewController *)controller didFinishWithSave:(BOOL)save;

@end