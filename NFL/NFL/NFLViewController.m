//
//  ViewController.m
//  NFL
//
//  Created by Barbara Brown on 10/10/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "NFLViewController.h"
#import "NFLCell.h"
#import "NFLWebViewController.h"

@interface NFLViewController ()

@property (nonatomic, strong) NSString *websiteURL;
@property (nonatomic, strong) NSString *teamName;

@end

@implementation NFLViewController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    
    //---------------------------------------------------------------------------------------------------
    
    // filePath is declared as a local variable of character string type
    NSString *filePath;   // File path to the plist file in the application's main bundle (project folder)
    
    //---------------------------------------------------------------------------------------------------
    // Obtain the file path to the Landmarks.plist file
    filePath = [[NSBundle mainBundle] pathForResource:@"NFL" ofType:@"plist"];
    self.nflDict = [[NSDictionary alloc] initWithContentsOfFile:filePath];
    
    /*
     Send allKeys message to the dictionary object and obtain all of the keys (landmark names) in an array.
     Send the sortedArrayUsingSelector: message to that array. It returns an array that lists the receiver’s
     elements in ascending order, as determined by the comparison method specified by a given selector.
     */
    self.nflCodes = [[self.nflDict allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
    [super viewDidLoad];   // Inform super
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Table View Data Source Methods

// Although the default is 1; it is still good to include this method for clarity
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Asks the data source to return the number of rows in a given section.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.nflCodes count];
}

// Asks the data source to return a cell to insert in a particular table view location
- (NFLCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger rowNumber = [indexPath row];
    
    NSString *nflCode = [self.nflCodes objectAtIndex:rowNumber];
    NSArray *nflData = [self.nflDict objectForKey:nflCode];
    
    NFLCell *cell = (NFLCell *)[tableView dequeueReusableCellWithIdentifier:@"NFLCellType"];
    
    
    cell.teamNameShortLabel.text = nflCode;
    cell.logoImageView.image = [UIImage imageNamed:[nflData objectAtIndex:0]];
    cell.teamNameLabel.text = [nflData objectAtIndex:1];
    cell.conferanceLabel.text = [nflData objectAtIndex:2];
    cell.stadiumLabel.text = [nflData objectAtIndex:3];
    
    return cell;
}


#pragma mark - Table View Delegate Methods

//       Asks the table view delegate to return the height of a given row.
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kTableViewRowHeight;
}


//       Informs the table view delegate that the table view is about to display a cell for a particular row.
//       Just before the cell is displayed, we change the color of the cell's background.

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //   We use the modulus operator % to find the remainder of the division of row number by 2.
    //   If the remainder is 0, it is an even number (use MintCream); otherwise, it is an odd number (use OldLace).
    
    if (indexPath.row % 2 == 0) {
        // Set even numbered row background color to MintCream, #F5FFFA  245,255,250
        cell.backgroundColor = MINT_CREAM;
    } else {
        // Set odd numbered row background color to OldLace, #FDF5E6   253,245,230
        cell.backgroundColor = OLD_LACE;
    }
}


// Informs the table view delegate that the specified row is now selected.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger rowNumber = [indexPath row];
    NSString *nflCode = [self.nflCodes objectAtIndex:rowNumber];
    
    self.nflData = [self.nflDict objectForKey:nflCode];
    
    self.teamName = [self.nflData objectAtIndex:1];
    self.websiteURL = [self.nflData objectAtIndex:4];
    
    // Perform the segue named CountryMapView
    [self performSegueWithIdentifier:@"NFLWebView" sender:self];
}


// Informs the table view delegate that the user tapped the detail accessory button
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger rowNumber = [indexPath row];
    NSString *nflCode = [self.nflCodes objectAtIndex:rowNumber];
    
    self.nflData = [self.nflDict objectForKey:nflCode];
    self.teamName = [self.nflData objectAtIndex:1];
    NSArray *nflData = [self.nflDict objectForKey:nflCode];
    NSString *stadiumName = [nflData objectAtIndex:3];
    NSString *teamStadiumNameWithNoSpaces = [stadiumName stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSString *teamStadiumNameQuery = [teamStadiumNameWithNoSpaces stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
    // Compose the query URL for Google's map API
    NSString *query = [NSString stringWithFormat:@"http://maps.google.com/?q=%@", teamStadiumNameQuery];
    
    self.websiteURL = query;
    
    // Perform the segue named CountryMapView
    [self performSegueWithIdentifier:@"NFLWebView" sender:self];
}


#pragma mark - Preparing for Segue

// This method is called by the system whenever you invoke the method performSegueWithIdentifier:sender:
// You never call this method. It is invoked by the system.
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"NFLWebView"]) {
        
        // Obtain the object reference of the destination view controller
        NFLWebViewController *nflWebViewController = [segue destinationViewController];
        
        // Pass the data objects teamName and websiteURL to the downstream view controller NFLWebViewController
        nflWebViewController.teamName = self.teamName;
        nflWebViewController.websiteURL = self.websiteURL;
    }
}

@end