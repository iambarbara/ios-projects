//
//  DetailViewController.h
//  NFL
//
//  Created by Barbara Brown on 10/17/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *nflWebView;

// The data object dataForSelectedTeam is passsed down from the previous view controller
@property (nonatomic, strong) NSArray *dataForSelectedTeam;

@end
