//
//  NFLCell.h
//  NFL
//
//  Created by Barbara Brown on 10/17/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NFLCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *teamNameShortLabel;
@property (nonatomic, strong) IBOutlet UILabel *teamNameLabel;
@property (nonatomic, strong) IBOutlet UIImageView *logoImageView;
@property (nonatomic, strong) IBOutlet UILabel *conferanceLabel;
@property (nonatomic, strong) IBOutlet UILabel *stadiumLabel;

@end
