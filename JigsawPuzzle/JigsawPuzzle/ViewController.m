//
//  ViewController.m
//  JigsawPuzzle
//
//  Created by Barbara Brown on 11/4/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
//@property (nonatomic, strong) UIImageView *imageViewL;
//@property (nonatomic, strong) UIImageView *imageViewR;
@property (strong, nonatomic) NSDate *currentDate;
@end

@implementation ViewController

/*
 A geometric translation moves every point of our UIImageView object by the same amount
 in a given direction when it is panned (dragged) from one location to another.
 We store the (x, y) coordinates of the last translation of imageViewj into the
 static variable lastTranslationj, where j=1,2,3,4
 
 CGPoint is a structure that contains the (x, y) coordinate values
 */

static CGPoint lastTranslation;

- (void)viewDidLoad
{
    // Sounds
    NSURL *soundFileURL = [[NSBundle mainBundle] URLForResource: @"clickSound" withExtension: @"wav"];
    self.clickPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    soundFileURL = [[NSBundle mainBundle] URLForResource: @"applaudSound" withExtension: @"wav"];
    self.congratsPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"puzzlePieceCenterCoordinates" ofType:@"plist"];
    
    self.puzzleDict = [[NSDictionary alloc] initWithContentsOfFile:path];
    self.puzzleArray = [[self.puzzleDict allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
    [self drawRectangle];
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)drawRectangle
{
    // Obtain the height, width information of the super view.
    CGFloat height = self.view.frame.size.height;
    CGFloat width = self.view.frame.size.width;
    CGFloat halfHeight = self.view.frame.size.height/2;
    CGFloat halfWidth = self.view.frame.size.width/2;
    
    // Obtain the size of the full puzzle image: jigsawPuzzlePhoto.png
    CGFloat imageWidthInPixels = 603;
    CGFloat imageHeightInPixels = 453;
    
    // Calculate the position where rectangle canvas is placed. (Top left corner of rect)
    CGFloat positionRectY = (height/2) - (imageWidthInPixels/2);
    CGFloat positionRectX = (width/2) - (imageHeightInPixels/2);
    
    // Geometric objects (e.g., rectangle) can be drawn on top of a UIImageView object
    // Therefore, create a UIImageView object as your canvas with a size of 1024x768 to hold the drawings
    // Specify in the .h file: @property (strong, nonatomic)  UIImageView *backgroundRect;
    self.backgroundRect = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, height, width)];
    
    // Create a bitmap-based graphics context and make it the current context.
    UIGraphicsBeginImageContext(self.view.frame.size);
    
    // Draw the entire image in the specified rectangle, scaling it as needed to fit.
    [self.backgroundRect.image drawInRect:CGRectMake(0, 0, width, height)];
    
    // Obtain the current graphics context
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Create a new empty path in the current graphics context
    CGContextBeginPath(context);
    
    // Draw a rectangle with the given position and size
    CGContextStrokeRect(context, CGRectMake(positionRectX, positionRectY, imageHeightInPixels, imageWidthInPixels));
    
    // Set the background canvas to the contents of the current bitmap graphics context.
    self.backgroundRect.image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Remove the current bitmap-based graphics context from the top of the stack.
    UIGraphicsEndImageContext();
    
    // Set the background canvas center
    [self.backgroundRect setCenter:CGPointMake(halfHeight, halfWidth)];
    
    [self.view addSubview:self.backgroundRect];
}

- (IBAction)newGameButton:(id)sender {
    [self.puzzleImageView setHidden:YES];
    [self.congratsImageView setHidden:YES];
    [self.bottomLabel setText:@""];
    
    [self.navBarTitle setText:@""];
    [self startTimer];
    
    //float rand = 0 + arc4random_uniform(200);
    
    /**************************************************************************************************
     *  Panning (Dragging) is a continuous gesture, which                                             *
     *                                                                                                *
     *   (a) begins when the user starts panning while one or more fingers are pressed down, and      *
     *   (b) ends when all fingers are lifted.                                                        *
     *                                                                                                *
     *  Panning has the following 3 states:                                                           *
     *                                                                                                *
     *  UIGestureRecognizerStateBegan                                 UIGestureRecognizerStateEnded   *
     *              |                                                                |                *
     *              |                UIGestureRecognizerStateChanged                 |                *
     *              V                                                                V                *
     *  time --------------------------------------------------------------------------------------   *
     *         touch begins                                                     touch ends            *
     *************************************************************************************************/
    
    //--------------------------------------------------------------
    // Create Panning (Dragging) Gesture Recognizer for Image View 1
    UIPanGestureRecognizer *panRecognizer1 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanning:)];
    
    // Add Panning (Dragging) Gesture Recognizer to Image View 1
    [self.p1 addGestureRecognizer:panRecognizer1];
    
    //--------------------------------------------------------------
    // Create Panning (Dragging) Gesture Recognizer for Image View 2
    UIPanGestureRecognizer *panRecognizer2 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanning:)];
    
    // Add Panning (Dragging) Gesture Recognizer to Image View 2
    [self.p2 addGestureRecognizer:panRecognizer2];
    
    //--------------------------------------------------------------
    // Create Panning (Dragging) Gesture Recognizer for Image View 3
    UIPanGestureRecognizer *panRecognizer3 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanning:)];
    
    // Add Panning (Dragging) Gesture Recognizer to Image View 3
    [self.p3 addGestureRecognizer:panRecognizer3];
    
    //--------------------------------------------------------------
    // Create Panning (Dragging) Gesture Recognizer for Image View 4
    UIPanGestureRecognizer *panRecognizer4 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanning:)];
    
    // Add Panning (Dragging) Gesture Recognizer to Image View 4
    [self.p4 addGestureRecognizer:panRecognizer4];
    
    //--------------------------------------------------------------
    // Create Panning (Dragging) Gesture Recognizer for Image View 5
    UIPanGestureRecognizer *panRecognizer5 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanning:)];
    
    // Add Panning (Dragging) Gesture Recognizer to Image View 5
    [self.p5 addGestureRecognizer:panRecognizer5];
    
    //--------------------------------------------------------------
    // Create Panning (Dragging) Gesture Recognizer for Image View 6
    UIPanGestureRecognizer *panRecognizer6 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanning:)];
    
    // Add Panning (Dragging) Gesture Recognizer to Image View 6
    [self.p6 addGestureRecognizer:panRecognizer6];
    
    //--------------------------------------------------------------
    // Create Panning (Dragging) Gesture Recognizer for Image View 7
    UIPanGestureRecognizer *panRecognizer7 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanning:)];
    
    // Add Panning (Dragging) Gesture Recognizer to Image View 7
    [self.p7 addGestureRecognizer:panRecognizer7];
    
    //--------------------------------------------------------------
    // Create Panning (Dragging) Gesture Recognizer for Image View 8
    UIPanGestureRecognizer *panRecognizer8 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanning:)];
    
    // Add Panning (Dragging) Gesture Recognizer to Image View 8
    [self.p8 addGestureRecognizer:panRecognizer8];
    
    //--------------------------------------------------------------
    // Create Panning (Dragging) Gesture Recognizer for Image View 9
    UIPanGestureRecognizer *panRecognizer9 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanning:)];
    
    // Add Panning (Dragging) Gesture Recognizer to Image View 9
    [self.p9 addGestureRecognizer:panRecognizer9];
    
    //--------------------------------------------------------------
    // Create Panning (Dragging) Gesture Recognizer for Image View 10
    UIPanGestureRecognizer *panRecognizer10 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanning:)];
    
    // Add Panning (Dragging) Gesture Recognizer to Image View 10
    [self.p10 addGestureRecognizer:panRecognizer10];
    
    //--------------------------------------------------------------
    // Create Panning (Dragging) Gesture Recognizer for Image View 11
    UIPanGestureRecognizer *panRecognizer11 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanning:)];
    
    // Add Panning (Dragging) Gesture Recognizer to Image View 11
    [self.p11 addGestureRecognizer:panRecognizer11];
    
    //--------------------------------------------------------------
    // Create Panning (Dragging) Gesture Recognizer for Image View 12
    UIPanGestureRecognizer *panRecognizer12 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanning:)];
    
    // Add Panning (Dragging) Gesture Recognizer to Image View 12
    [self.p12 addGestureRecognizer:panRecognizer12];
    
    // Move puzzle pieces to random locations and unhide
    self.p1.layer.frame = (CGRect){{arc4random_uniform(859), arc4random_uniform(598)}, self.p1.frame.size};
    [self.p1 setHidden:NO];
    self.p2.layer.frame = (CGRect){{arc4random_uniform(796), arc4random_uniform(589)}, self.p2.frame.size};
    [self.p2 setHidden:NO];
    self.p3.layer.frame = (CGRect){{arc4random_uniform(854), arc4random_uniform(597)}, self.p3.frame.size};
    [self.p3 setHidden:NO];
    self.p4.layer.frame = (CGRect){{arc4random_uniform(850), arc4random_uniform(592)}, self.p4.frame.size};
    [self.p4 setHidden:NO];
    self.p5.layer.frame = (CGRect){{arc4random_uniform(866), arc4random_uniform(581)}, self.p5.frame.size};
    [self.p5 setHidden:NO];
    self.p6.layer.frame = (CGRect){{arc4random_uniform(852), arc4random_uniform(579)}, self.p6.frame.size};
    [self.p6 setHidden:NO];
    self.p7.layer.frame = (CGRect){{arc4random_uniform(836), arc4random_uniform(581)}, self.p7.frame.size};
    [self.p7 setHidden:NO];
    self.p8.layer.frame = (CGRect){{arc4random_uniform(848), arc4random_uniform(604)}, self.p8.frame.size};
    [self.p8 setHidden:NO];
    self.p9.layer.frame = (CGRect){{arc4random_uniform(856), arc4random_uniform(615)}, self.p9.frame.size};
    [self.p9 setHidden:NO];
    self.p10.layer.frame = (CGRect){{arc4random_uniform(835), arc4random_uniform(615)}, self.p10.frame.size};
    [self.p10 setHidden:NO];
    self.p11.layer.frame = (CGRect){{arc4random_uniform(837), arc4random_uniform(614)}, self.p11.frame.size};
    [self.p11 setHidden:NO];
    self.p12.layer.frame = (CGRect){{arc4random_uniform(844), arc4random_uniform(612)}, self.p12.frame.size};
    [self.p12 setHidden:NO];
}

- (void)startTimer {
    self.startDate = [NSDate date];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0/10.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
    
}

- (void)stopTimer {
    [self.timer invalidate];
    //self.timer = nil;
    [self updateTimer];
}

- (void)updateTimer {
    // Create date
    self.currentDate = [NSDate date];
    NSTimeInterval timeInterval = [self.currentDate timeIntervalSinceDate:self.startDate];
    NSDate *timerDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    
    // Format date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss.SS"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];

    // Set label text
    NSString *timeString = [dateFormatter stringFromDate:timerDate];
    self.timerLabel.text = timeString;
}

#pragma mark - Panning (Dragging) Handling Methods

/*********************************************************
 *  Handle Panning (Dragging) Gesture  for Image View 1  *
 ********************************************************/
- (void)handlePanning:(UIPanGestureRecognizer *)recognizer {
   
    NSString *pp = [self.puzzleArray objectAtIndex:recognizer.view.tag];
    NSArray *coord = [self.puzzleDict objectForKey:pp];
    
    [self.view bringSubviewToFront:recognizer.view];
    
    CGPoint newTranslation = [recognizer translationInView:recognizer.view];
    
    recognizer.view.transform = CGAffineTransformMakeTranslation(lastTranslation.x + newTranslation.x,
                                                                 lastTranslation.y + newTranslation.y);
    recognizer.view.center = CGPointMake(recognizer.view.center.x+newTranslation.x, recognizer.view.center.y+newTranslation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];

    if (recognizer.state == UIGestureRecognizerStateEnded) {
        lastTranslation.x += newTranslation.x;
        lastTranslation.y += newTranslation.y;

        NSDecimalNumber *x = [coord objectAtIndex:0];
        NSDecimalNumber *y = [coord objectAtIndex:1];

        if (recognizer.view.center.x > x.floatValue-21 && recognizer.view.center.x < x.floatValue+21) {
            if (recognizer.view.center.y > y.floatValue-21 && recognizer.view.center.y < y.floatValue+21) {
                [recognizer.view setCenter:CGPointMake(x.floatValue, y.floatValue)];
                [self.clickPlayer play];
            }
        }
    }
    
    pp = [self.puzzleArray objectAtIndex:0];
    coord = [self.puzzleDict objectForKey:pp];
    NSDecimalNumber *x = [coord objectAtIndex:0];
    NSDecimalNumber *y = [coord objectAtIndex:1];
    if (self.p1.center.x == x.floatValue && self.p1.center.y == y.floatValue) {
        pp = [self.puzzleArray objectAtIndex:1];
        coord = [self.puzzleDict objectForKey:pp];
        x = [coord objectAtIndex:0];
        y = [coord objectAtIndex:1];
        if (self.p10.center.x == x.floatValue && self.p10.center.y == y.floatValue) {
            pp = [self.puzzleArray objectAtIndex:2];
            coord = [self.puzzleDict objectForKey:pp];
            x = [coord objectAtIndex:0];
            y = [coord objectAtIndex:1];
            if (self.p11.center.x == x.floatValue && self.p11.center.y == y.floatValue) {
                pp = [self.puzzleArray objectAtIndex:3];
                coord = [self.puzzleDict objectForKey:pp];
                x = [coord objectAtIndex:0];
                y = [coord objectAtIndex:1];
                if (self.p12.center.x == x.floatValue && self.p12.center.y == y.floatValue) {
                    pp = [self.puzzleArray objectAtIndex:4];
                    coord = [self.puzzleDict objectForKey:pp];
                    x = [coord objectAtIndex:0];
                    y = [coord objectAtIndex:1];
                    if (self.p2.center.x == x.floatValue && self.p2.center.y == y.floatValue) {
                        pp = [self.puzzleArray objectAtIndex:5];
                        coord = [self.puzzleDict objectForKey:pp];
                        x = [coord objectAtIndex:0];
                        y = [coord objectAtIndex:1];
                        if (self.p3.center.x == x.floatValue && self.p3.center.y == y.floatValue) {
                            pp = [self.puzzleArray objectAtIndex:6];
                            coord = [self.puzzleDict objectForKey:pp];
                            x = [coord objectAtIndex:0];
                            y = [coord objectAtIndex:1];
                            if (self.p4.center.x == x.floatValue && self.p4.center.y == y.floatValue) {
                                pp = [self.puzzleArray objectAtIndex:7];
                                coord = [self.puzzleDict objectForKey:pp];
                                x = [coord objectAtIndex:0];
                                y = [coord objectAtIndex:1];
                                if (self.p5.center.x == x.floatValue && self.p5.center.y == y.floatValue) {
                                    pp = [self.puzzleArray objectAtIndex:8];
                                    coord = [self.puzzleDict objectForKey:pp];
                                    x = [coord objectAtIndex:0];
                                    y = [coord objectAtIndex:1];
                                    if (self.p6.center.x == x.floatValue && self.p6.center.y == y.floatValue) {
                                        pp = [self.puzzleArray objectAtIndex:9];
                                        coord = [self.puzzleDict objectForKey:pp];
                                        x = [coord objectAtIndex:0];
                                        y = [coord objectAtIndex:1];
                                        if (self.p7.center.x == x.floatValue && self.p7.center.y == y.floatValue) {
                                            pp = [self.puzzleArray objectAtIndex:10];
                                            coord = [self.puzzleDict objectForKey:pp];
                                            x = [coord objectAtIndex:0];
                                            y = [coord objectAtIndex:1];
                                            if (self.p8.center.x == x.floatValue && self.p8.center.y == y.floatValue) {
                                                pp = [self.puzzleArray objectAtIndex:11];
                                                coord = [self.puzzleDict objectForKey:pp];
                                                x = [coord objectAtIndex:0];
                                                y = [coord objectAtIndex:1];
                                                if (self.p9.center.x == x.floatValue && self.p9.center.y == y.floatValue) {
                                                    // GAME WON
                                                    [self stopTimer];
                                                    [self.navBarTitle setText:@"Tap New Game to Play Again!"];
                                                    [self.congratsImageView setHidden:NO];
                                                    [self.congratsPlayer play];
                                                    
                                                    // Get the location (x, y) of where the swipe started
                                                    CGPoint locationl = CGPointMake(152,546);
                                                    CGPoint locationr = CGPointMake(865,546);
                                                    
                                                    self.imageViewL.image = [UIImage imageNamed:@"rocketFlyingUp"];
                                                    self.imageViewR.image = [UIImage imageNamed:@"rocketFlyingUp"];
                                                    
                                                    // Set the image view center to be the swipe start location where animation will begin
                                                    self.imageViewL.center = locationl;
                                                    self.imageViewR.center = locationr;
                                                    
                                                    // Make the image entirely visible at the start of animation
                                                    self.imageViewL.alpha = 1.0;
                                                    self.imageViewR.alpha = 1.0;
                                                    
                                                    // Specify the location where the animation will be carried out to, i.e., where the image will fade out
                                                    locationl.y -= 216;
                                                    locationr.y -= 216;
                                                    
                                                    [UIView beginAnimations:nil context:NULL];
                                                    [UIView setAnimationDuration:1.30];
                                                    
                                                    // Set the image view center to the location where the animation will end
                                                    self.imageViewL.center = locationl;
                                                    self.imageViewR.center = locationr;
                                                    
                                                    // Make the image entirely invisible (fade out) at the end of animation
                                                    self.imageViewL.alpha = 0.0;
                                                    self.imageViewR.alpha = 0.0;
                                                    
                                                    [UIView commitAnimations];
                                                    
                                                    [self.p1 setHidden:YES];
                                                    [self.p2 setHidden:YES];
                                                    [self.p3 setHidden:YES];
                                                    [self.p4 setHidden:YES];
                                                    [self.p5 setHidden:YES];
                                                    [self.p6 setHidden:YES];
                                                    [self.p7 setHidden:YES];
                                                    [self.p8 setHidden:YES];
                                                    [self.p9 setHidden:YES];
                                                    [self.p10 setHidden:YES];
                                                    [self.p11 setHidden:YES];
                                                    [self.p12 setHidden:YES];
                                                    
                                                    [self.puzzleImageView setHidden:NO];
                                                    [self.view bringSubviewToFront:self.congratsImageView];
                                                    
                                                    NSTimeInterval timeInterval = [self.currentDate timeIntervalSinceDate:self.startDate];
                                                    
                                                    [self.bottomLabel setHidden:NO];
                                                    
                                                    if (timeInterval <= 20)
                                                        [self.bottomLabel setText:@"You did outstanding!"];
                                                    else if (timeInterval > 20 && timeInterval <= 40)
                                                        [self.bottomLabel setText:@"You did excellent!"];
                                                    else if (timeInterval > 40 && timeInterval <= 120)
                                                        [self.bottomLabel setText:@"You did good."];
                                                    else if (timeInterval > 120 && timeInterval <= 180)
                                                        [self.bottomLabel setText:@"You did satisfactory."];
                                                    else if (timeInterval > 180)
                                                        [self.bottomLabel setText:@"You did slow."];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@end