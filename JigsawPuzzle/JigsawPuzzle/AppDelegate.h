//
//  AppDelegate.h
//  JigsawPuzzle
//
//  Created by Barbara Brown on 11/4/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
