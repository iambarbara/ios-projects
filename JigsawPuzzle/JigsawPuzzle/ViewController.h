//
//  ViewController.h
//  JigsawPuzzle
//
//  Created by Barbara Brown on 11/4/13.
//  Copyright (c) 2013 Barbara Brown. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController
// Methods
- (void)handlePanning:(UIPanGestureRecognizer *)recognizer;
//- (void)drawRectangle;

// Variables
@property (nonatomic, strong) NSDictionary *puzzleDict;
@property (nonatomic, strong) NSArray *puzzleArray;

@property (strong, nonatomic) IBOutlet UIView *borderView;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundRect;
@property (strong, nonatomic) IBOutlet UIImageView *puzzleImageView;
@property (strong, nonatomic) IBOutlet UILabel *timerLabel;
@property (strong, nonatomic) IBOutlet UILabel *navBarTitle;
@property (strong, nonatomic) IBOutlet UILabel *bottomLabel;
@property (nonatomic, strong) AVAudioPlayer *clickPlayer;
@property (nonatomic, strong) AVAudioPlayer *congratsPlayer;
@property (strong, nonatomic) IBOutlet UIImageView *congratsImageView;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewL;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewR;

// Puzzle Piece Image View
@property (strong, nonatomic) IBOutlet UIImageView *p1;
@property (strong, nonatomic) IBOutlet UIImageView *p2;
@property (strong, nonatomic) IBOutlet UIImageView *p3;
@property (strong, nonatomic) IBOutlet UIImageView *p4;
@property (strong, nonatomic) IBOutlet UIImageView *p5;
@property (strong, nonatomic) IBOutlet UIImageView *p6;
@property (strong, nonatomic) IBOutlet UIImageView *p7;
@property (strong, nonatomic) IBOutlet UIImageView *p8;
@property (strong, nonatomic) IBOutlet UIImageView *p9;
@property (strong, nonatomic) IBOutlet UIImageView *p10;
@property (strong, nonatomic) IBOutlet UIImageView *p11;
@property (strong, nonatomic) IBOutlet UIImageView *p12;

// Timer Variables
@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) NSDate *startDate;

- (IBAction)newGameButton:(id)sender;

@end
